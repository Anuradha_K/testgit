package com.example.cards;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {
	
	ImageView imgFront;
	ImageView imgBack;
	Button btnFlip;

	boolean isBackVisible = false; // Boolean variable to check if the back
									// image is visible currently

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		imgFront = (ImageView) findViewById(R.id.imgFront);
		imgBack = (ImageView) findViewById(R.id.imgBack);
		btnFlip = (Button) findViewById(R.id.btnFlip);

		final AnimatorSet setRightOut = (AnimatorSet) AnimatorInflater
				.loadAnimator(getApplicationContext(), R.anim.flip_right_out);

		final AnimatorSet setLeftIn = (AnimatorSet) AnimatorInflater
				.loadAnimator(getApplicationContext(), R.anim.flip_left_in);

		btnFlip.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!isBackVisible) {
					setRightOut.setTarget(imgFront);
					setLeftIn.setTarget(imgBack);
					setRightOut.start();
					setLeftIn.start();
					isBackVisible = true;
				} else {
					setRightOut.setTarget(imgBack);
					setLeftIn.setTarget(imgFront);
					setRightOut.start();
					setLeftIn.start();
					isBackVisible = false;
				}
			}
		});
	}
}